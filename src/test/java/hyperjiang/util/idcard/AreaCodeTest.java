package hyperjiang.util.idcard;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AreaCodeTest {

    @Test
    public void testLoad() {
        assertEquals("普宁市", AreaCode.load("445281"));
    }

    @Test(expected = NullPointerException.class)
    public void testNotExists() {
        AreaCode.load("000000");
    }
}
