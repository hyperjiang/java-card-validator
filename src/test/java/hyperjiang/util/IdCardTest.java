package hyperjiang.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.junit.Test;

public class IdCardTest {

    public IdCard idCard;

    public IdCardTest() {
        this.idCard = new IdCard("44528119991001005X");
    }

    @Test
    public void testIsValid() {
        assertEquals(false, this.idCard.isValid());
    }

    @Test
    public void testGetBirthday() {
        assertEquals("19991001", this.idCard.getBirthday());
    }

    @Test
    public void testGetProvinceCode() {
        assertEquals("440000", this.idCard.getProvinceCode());
    }

    @Test
    public void testGetCityCode() {
        assertEquals("445200", this.idCard.getCityCode());
    }

    @Test
    public void testGetCountyCode() {
        assertEquals("445281", this.idCard.getCountyCode());
    }

    @Test
    public void testGetProvince() {
        assertEquals("广东省", this.idCard.getProvince());
    }

    @Test
    public void testGetCity() {
        assertEquals("普宁市", this.idCard.getCity());
    }

    @Test
    public void testGetCounty() {
        assertEquals("普宁市", this.idCard.getCounty());
    }

    @Test
    public void testGetAddress() {
        assertEquals("广东省普宁市", this.idCard.getAddress());
    }

    @Test
    public void testGetCardType() {
        assertEquals(1, this.idCard.getCardType());
    }

    @Test
    public void testGetGender() {
        assertEquals('男', this.idCard.getGender());
    }

    @Test
    public void testGetAge() {
        Calendar cal = Calendar.getInstance();
        int currentYear = cal.get(Calendar.YEAR);
        assertTrue(currentYear - 1999 >= this.idCard.getAge());
    }

    @Test
    public void testGetConstellation() {
        assertEquals("天秤", this.idCard.getConstellation());
    }
}
