package hyperjiang.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BankCardTest {

    public BankCard bankCard;

    public BankCardTest() {
        this.bankCard = new BankCard("6225700000000000");
    }

    @Test
    public void testGetBankCode() {
        assertEquals("CEB", this.bankCard.getBankCode());
    }

    @Test
    public void testGetBankName() {
        assertEquals("中国光大银行", this.bankCard.getBankName());
    }

    @Test
    public void testGetCardType() {
        assertEquals("CC", this.bankCard.getCardType());
    }

    @Test
    public void testGetCardTypeName() {
        assertEquals("信用卡", this.bankCard.getCardTypeName());
    }
}
