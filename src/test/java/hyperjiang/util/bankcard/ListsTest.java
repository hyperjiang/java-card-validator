package hyperjiang.util.bankcard;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ListsTest {

    @Test
    public void testGetBankByCode() {
        assertEquals("中国工商银行", Lists.getBankByCode("ICBC").getName());
    }

    @Test
    public void testGetBank() {
        assertEquals("中国工商银行", Lists.getBank("6222003602106934000").getName());
    }
}