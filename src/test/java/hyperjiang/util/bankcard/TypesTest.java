package hyperjiang.util.bankcard;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TypesTest {

    @Test
    public void testGetName() {
        assertEquals("储蓄卡", Types.getName("DC"));
    }

}