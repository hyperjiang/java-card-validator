package hyperjiang.util.idcard;

import java.io.IOException;
import java.io.InputStream;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class AreaCode {

    public static JSONObject map;

    static {
        try {
            InputStream is = AreaCode.class.getResourceAsStream("codes.json");
            map = JSON.parseObject(is, JSONObject.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getMap() {
        return map;
    }

    public static String load(String code) {
        return map.get(code).toString();
    }

}
