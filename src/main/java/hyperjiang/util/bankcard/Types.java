package hyperjiang.util.bankcard;

import java.util.HashMap;

@SuppressWarnings("serial")
public class Types {

    public static HashMap<String, String> map;

    static {
        map = new HashMap<String, String>() {
            {
                put("DC", "储蓄卡");
                put("CC", "信用卡");
                put("SCC", "准贷记卡");
                put("PC", "预付费卡");
            }
        };
    }

    public static String getName(String cardType) {
        return Types.map.getOrDefault(cardType, "");
    }

}