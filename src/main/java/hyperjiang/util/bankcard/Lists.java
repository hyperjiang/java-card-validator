package hyperjiang.util.bankcard;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class Lists {

    public static JSONObject map;

    static {
        try {
            InputStream is = Lists.class.getResourceAsStream("banks.json");
            map = JSON.parseObject(is, JSONObject.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 根据银行代码获取银行信息
    public static Bank getBankByCode(String code) {
        return JSON.parseObject(map.get(code).toString(), Bank.class);
    }

    // 根据银行卡号获取所属银行
    public static Bank getBank(String cardNo) {
        for (Entry<String, Object> entry : map.entrySet()) {
            Bank bank = JSON.parseObject(entry.getValue().toString(), Bank.class);
            List<Pattern> patterns = bank.getPatterns();
            Boolean found = false;
            for (Pattern pattern : patterns) {
                java.util.regex.Pattern r = java.util.regex.Pattern.compile(pattern.getReg());
                Matcher m = r.matcher(cardNo);
                if (m.find()) {
                    found = true;
                    bank.setCardType(pattern.getType());
                }
            }
            if (found) {
                return bank;
            }
        }
        return null;
    }
}
