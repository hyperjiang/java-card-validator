package hyperjiang.util.bankcard;

import java.util.ArrayList;
import java.util.List;

public class Bank {

    public String code;
    public String name;
    public String cardType;
    public List<Pattern> patterns = new ArrayList<Pattern>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Pattern> getPatterns() {
        return patterns;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setPatterns(List<Pattern> patterns) {
        this.patterns = patterns;
    }

    public void addPattern(Pattern pattern) {
        patterns.add(pattern);
    }
}
