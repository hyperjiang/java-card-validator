package hyperjiang.util;

import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import hyperjiang.util.bankcard.Bank;
import hyperjiang.util.bankcard.Lists;
import hyperjiang.util.bankcard.Types;

public class BankCard {

    public String cardNo; // 银行卡号
    public String bankCode; // 银行编码
    public String bankName; // 银行名称
    public String cardType; // 卡号类型
    public String cardTypeName; // 卡号类型描述

    // 构造函数
    public BankCard(String cardNo) {
        this.cardNo = cardNo.trim();
        this.parse();
    }

    // 解析银行卡
    public void parse() {
        Bank bank = Lists.getBank(cardNo);
        if (bank != null) {
            bankCode = bank.getCode();
            bankName = bank.getName();
            cardType = bank.getCardType();
            cardTypeName = Types.getName(cardType);
        } else {
            alipay();
        }
    }

    // 获取银行编码
    public String getBankCode() {
        return bankCode;
    }

    // 获取银行名称
    public String getBankName() {
        return bankName;
    }

    // 获取卡号类型
    public String getCardType() {
        return cardType;
    }

    // 获取卡号类型描述
    public String getCardTypeName() {
        return cardTypeName;
    }

    // 获取支付宝提供的银行图标
    public String getBankIcon() {
        return "https://apimg.alipay.com/combo.png?d=cashier&t=" + this.bankCode;
    }

    // 通过支付宝接口获取卡信息
    public void alipay() {

        CloseableHttpClient httpclient = HttpClients.createDefault();

        try {
            URI uri = new URIBuilder().setScheme("https").setHost("ccdcapi.alipay.com")
                    .setPath("/validateAndCacheCardInfo.json").setParameter("_input_charset", "utf-8")
                    .setParameter("cardNo", this.cardNo).setParameter("cardBinCheck", "true").build();
            HttpGet httpget = new HttpGet(uri);

            CloseableHttpResponse response = httpclient.execute(httpget);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                try {
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        String content = EntityUtils.toString(entity);
                        JSONObject map = JSON.parseObject(content);
                        if (map.getBoolean("validated")) {
                            bankCode = map.getString("bank");
                            cardType = map.getString("cardType");
                            cardTypeName = Types.getName(cardType);
                            bankName = Lists.getBankByCode(bankCode).getName();
                        }
                    }
                } finally {
                    response.close();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
