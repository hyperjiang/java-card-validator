package hyperjiang.util;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.DateUtils;

import hyperjiang.util.idcard.AreaCode;

public class IdCard {

    public final static int MAINLAND = 1; // 证件类型 1.大陆
    public final static int GAT = 2; // 证件类型 2.港澳台
    public final static int MAX_AGE = 200; // 最大岁数，会有人超过200岁吗？

    public String idNo; // 身份证号
    public Boolean isValid; // 是否合法
    public String birthday; // 生日
    public String provinceCode; // 省级邮编
    public String cityCode; // 市级邮编
    public String countyCode; // 县级邮编
    public String province; // 省
    public String city; // 市
    public String county; // 县
    public Character gender; // 性别
    public Integer age; // 年龄
    public String constellation; // 星座
    public Integer version; // 身份证版本

    private final int[] modulus = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 }; // 身份证号码相乘系数
    private final char[] mapLastIdcardNo = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' }; // 身份证最后一位号码

    // 710000:台湾省
    // 810000:香港特别行政区
    // 820000:澳门特别行政区
    private final String[] gatMap = { "710000", "810000", "820000" };

    // 构造函数
    public IdCard(String idNo) {
        this.idNo = idNo.trim().toUpperCase();
        isValid = validate();
    }

    // 判断身份证是否合法
    public Boolean isValid() {
        return isValid;
    }

    // 获取生日
    public String getBirthday() {
        if (birthday == null) {
            if (getVersion() == 1) {
                birthday = "19" + idNo.substring(6, 12);
            } else if (getVersion() == 2) {
                birthday = idNo.substring(6, 14);
            }
        }
        return birthday;
    }

    // 获取省级编码
    public String getProvinceCode() {
        if (provinceCode == null) {
            provinceCode = idNo.substring(0, 2) + "0000";
        }
        return provinceCode;
    }

    // 获取市级编码
    public String getCityCode() {
        if (cityCode == null) {
            cityCode = idNo.substring(0, 4) + "00";
        }
        return cityCode;
    }

    // 获取县级编码
    public String getCountyCode() {
        if (countyCode == null) {
            countyCode = idNo.substring(0, 6);
        }
        return countyCode;
    }

    // 获取身份证类型：1 大陆，2 港澳台
    public int getCardType() {
        return ArrayUtils.contains(gatMap, getProvinceCode()) ? GAT : MAINLAND;
    }

    // 获取省份
    public String getProvince() {
        if (province == null) {
            province = AreaCode.load(getProvinceCode());
        }
        return province;
    }

    // 获取城市
    public String getCity() {
        if (city == null) {
            city = AreaCode.load(getCountyCode());
        }
        return city;
    }

    // 获取县区
    public String getCounty() {
        if (county == null) {
            county = AreaCode.load(getCountyCode());
        }
        return county;
    }

    // 获取性别
    public char getGender() {
        if (gender == null) {
            int genderChar = 0;
            if (getVersion() == 1) {
                genderChar = Character.getNumericValue(idNo.charAt(14));
            } else if (getVersion() == 2) {
                genderChar = Character.getNumericValue(idNo.charAt(16));
            }
            gender = genderChar % 2 == 0 ? '女' : '男'; // 奇数表示男性，偶数表示女性
        }

        return gender;
    }

    // 获取年龄
    public int getAge() {
        if (this.age == null) {
            String birthday = getBirthday();

            int year = Integer.parseInt(birthday.substring(0, 4));
            int month = Integer.parseInt(birthday.substring(4, 6));
            int date = Integer.parseInt(birthday.substring(6, 8));

            Calendar cal = Calendar.getInstance();
            int currentYear = cal.get(Calendar.YEAR);
            int currentMonth = cal.get(Calendar.MONTH) + 1;
            int currentDate = cal.get(Calendar.DATE);

            int age = currentYear - year;
            if (currentMonth < month || (currentMonth == month && currentDate < date)) {
                age--;
            }

            this.age = age;
        }

        return this.age;
    }

    // 获取星座
    public String getConstellation() {
        if (this.constellation == null) {

            String constellation = "";

            String birthday = this.getBirthday();
            int month = Integer.parseInt(birthday.substring(4, 6));
            int date = Integer.parseInt(birthday.substring(6, 8));
            switch (month) {
            case 1:
                constellation = date >= 21 ? "水瓶" : "摩羯";
                break;
            case 2:
                constellation = date >= 20 ? "双鱼" : "水瓶";
                break;
            case 3:
                constellation = date >= 21 ? "白羊" : "双鱼";
                break;
            case 4:
                constellation = date >= 21 ? "金牛" : "白羊";
                break;
            case 5:
                constellation = date >= 22 ? "双子" : "金牛";
                break;
            case 6:
                constellation = date >= 23 ? "巨蟹" : "双子";
                break;
            case 7:
                constellation = date >= 24 ? "狮子" : "巨蟹";
                break;
            case 8:
                constellation = date >= 24 ? "处女" : "狮子";
                break;
            case 9:
                constellation = date >= 24 ? "天秤" : "处女";
                break;
            case 10:
                constellation = date >= 24 ? "天蝎" : "天秤";
                break;
            case 11:
                constellation = date >= 23 ? "射手" : "天蝎";
                break;
            case 12:
                constellation = date >= 22 ? "摩羯" : "射手";
                break;
            }

            this.constellation = constellation;
        }

        return this.constellation;
    }

    // 获取地址
    public String getAddress() {
        String address = getProvince();
        if (address != getCity()) {
            address += getCity();
        }
        if (getCity() != getCounty()) {
            address += getCounty();
        }
        return address;
    }

    // 获取身份证版本，第一代身份证是15位，第二代身份证是18位
    public Integer getVersion() {
        if (version != null) {
            return version;
        }

        if (idNo.length() == 15) {
            version = 1;
        } else if (idNo.length() == 18) {
            version = 2;
        } else {
            version = 0;
        }

        return version;
    }

    // 正则匹配
    protected Boolean match(String pattern, String input) {
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(input);
        return m.find();
    }

    // 验证身份证有效性
    protected Boolean validate() {

        if (getVersion() == 1) {
            if (!match("^\\d{15}$", idNo)) {
                return false;
            }
        } else if (getVersion() == 2) {
            if (!match("^\\d{17}[0-9X]{1}$", idNo)) {
                return false;
            }
        } else {
            return false;
        }

        // 验证生日
        if (!validateBirthday()) {
            return false;
        }

        // 第一代身份证验证到此为止
        if (getVersion() == 1) {
            return true;
        }

        // 身份证前17位数字分别和对应的系数相乘, 再对11进行求余, 得到最后一位
        int sum = 0;
        for (int i = 0; i < 17; i++) {
            sum = sum + Character.getNumericValue(idNo.charAt(i)) * modulus[i];
        }
        int index = sum % 11;

        return mapLastIdcardNo[index] == idNo.charAt(17);
    }

    // 验证生日格式是否正确
    protected Boolean validateBirthday() {

        String birthday = getBirthday();

        if (birthday.length() != 8) {
            return false;
        }

        int year = Integer.parseInt(birthday.substring(0, 4));
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        if (year > currentYear || year <= currentYear - MAX_AGE) {
            return false;
        }

        try {
            DateUtils.parseDateStrictly(birthday, "yyyyMMdd");
            return true;
        } catch (Exception ex) {
        }

        return false;
    }

}
