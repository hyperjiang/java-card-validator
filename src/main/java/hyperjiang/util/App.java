package hyperjiang.util;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        echo("请选择需要的功能：\n[0] 结束\n[1] 身份证解析\n[2] 银行卡解析");

        switch (sc.nextInt()) {
        case 1:
            echo("请输入身份证号：");
            String idNo = sc.next();
            IdCard idCard = new IdCard(idNo);
            echo("是否合法：" + (idCard.isValid() ? "是" : "否"));
            if (idCard.isValid()) {
                echo("生日：" + idCard.getBirthday());
                echo("年龄：" + idCard.getAge());
                echo("证件类型：" + idCard.getCardType());
                echo("性别：" + idCard.getGender());
                echo("邮编：" + idCard.getCountyCode());
                echo("星座：" + idCard.getConstellation());
                echo("省：" + idCard.getProvince());
                echo("市：" + idCard.getCity());
                echo("县：" + idCard.getCounty());
                echo("地址：" + idCard.getAddress());
                echo("版本：" + idCard.getVersion());
            }
            break;
        case 2:
            echo("请输入银行卡号：");
            String cardNo = sc.next();
            BankCard bankCard = new BankCard(cardNo);
            echo("银行编码：" + bankCard.getBankCode());
            echo("银行名称：" + bankCard.getBankName());
            echo("卡号类型：" + bankCard.getCardType());
            echo("卡号类型描述：" + bankCard.getCardTypeName());
            break;
        }

        sc.close();
    }

    public static void echo(String str) {
        System.out.println(str);
    }
}
