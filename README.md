# util
Some java tools

## Assembly

Use the following command:

```
mvn package
```

You will build the jar files in the target directory.

## Command line tool

After you build the jar file, you can execute it, e.g.

```
java -jar target/util-0.2-jar-with-dependencies.jar
```

Have fun with the tool!